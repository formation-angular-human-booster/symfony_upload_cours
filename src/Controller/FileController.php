<?php

namespace App\Controller;

use App\Entity\Fichier;
use App\Form\UploadFileType;
use App\Repository\FichierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{
    /**
     * @Route("/", name="file")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(UploadFileType::class, new Fichier());

        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()){
            $image = $form->get('fileName')->getData();
            $name = uniqid().'.'.$image->guessExtension();

            try {
                $image->move(
                    $this->getParameter('image_article_directory'),
                    $name
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            $fichier = new Fichier();
            $fichier->setDateAjoutFichier(new \DateTime());
            $fichier->setFileName($name);

            $em = $this->getDoctrine()->getManager();
            $em->persist($fichier);
            $em->flush();

            return $this->redirectToRoute('file');
        } else {
            return $this->render('file/index.html.twig', [
                'controller_name' => 'FileController',
                'form'=> $form->createView()
            ]);
        }

    }
}
