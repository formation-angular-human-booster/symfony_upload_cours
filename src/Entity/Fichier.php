<?php

namespace App\Entity;

use App\Repository\FichierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FichierRepository::class)
 */
class Fichier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAjoutFichier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getDateAjoutFichier(): ?\DateTimeInterface
    {
        return $this->dateAjoutFichier;
    }

    public function setDateAjoutFichier(\DateTimeInterface $dateAjoutFichier): self
    {
        $this->dateAjoutFichier = $dateAjoutFichier;

        return $this;
    }
}
